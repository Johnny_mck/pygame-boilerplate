import pygame

class BaseSprite(pygame.sprite.Sprite):

    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.Surface((50, 50))
        self.image.fill((0, 0, 0))
        self.rect = self.image.get_rect()
        self.rect.centerx = 50
        self.rect.centery = 50
        self.dx = 2
        self.dy = 2

    def checkKeys(self):
        keys = pygame.key.get_pressed()
        if keys[pygame.K_w]:
           self.rect.centery -= self.dy
        if keys[pygame.K_s]:
            self.rect.centery += self.dy
        if keys[pygame.K_a]:
            self.rect.centerx -= self.dx
        if keys[pygame.K_d]:
            self.rect.centerx += self.dy

    def update(self):
        pass
