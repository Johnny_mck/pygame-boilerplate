import pygame
import sprites
import sys

from pygame.locals import *

# CONSTANTS
# dimentions
WIDTH = 640
HEIGHT = 480

# colours
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)

pygame.init()

display = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("Hello, world!")

# entities
background = pygame.Surface(display.get_size())
thing = sprites.BaseSprite()

allSprites = pygame.sprite.Group(thing)

clock = pygame.time.Clock()

def main():
    while True:
        for event in pygame.event.get():
            if event.type == QUIT:
                sys.exit(0)

        background.fill(WHITE)
        display.blit(background, (0, 0))

        allSprites.clear(display, background)
        thing.checkKeys()
        allSprites.update()
        allSprites.draw(display)

        pygame.display.flip()

if __name__ == "__main__":
    main()
